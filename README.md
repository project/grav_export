# Grav Export

## Requirements

PHP v7.1 or above for the composer dependencies.
Drush
Working Drupal 7 site from which content will be exported.
R/W access to `public://` on the Drupal site.

## Installation

1. Download and move this plugin to your Drupal's `sites/all/modules/contrib` folder.
2. Run `composer install` within the `grav_export` folder to install dependencies.
3. Enable it via `drush en grav_export -y` or via the admin gui.
4. Run `drush grav_export_all` or `drush gravea` to export all items.  See other options below.
5. Exported files are located at `sites/default/files/grav_export/EXPORT`
6. The Grav plugin [https://github.com/david-szabo97/grav-plugin-admin-addon-user-manager](admin-addon-user-manager) is recommended to view and manage users.  

## Notes

If a field is not configured yet, the node exporter and content type exporter will display messages notifying about the missing fields.

## Exporting Users from Drupal

### Command

`drush grav_export_users` or `drush graveu` will generate Grav user account files.

### Results

* User accounts in the export folder under `EXPORT/accounts/`.  
  * Usernames will be padded to a minimum of 3 characters, maximum of 16.  
  * If a username is truncated or padded, the username will also have the Drupal uid to avoid collisions.
  * Passwords in each account are randomly generated, and have no connection with the respective Drupal account.  The password automatically switches to a hashed_password once the account authenticates for the first time.

### Importing Users to Grav

Copy the `EXPORT/accounts` folder to your `user` directory (e.g. username.yaml files should be placed at `user/accounts`).

## Exporting User Roles from Drupal

### Command

`drush grav_export_roles` or `drush graver` will generate a Grav groups.yaml file.

### Results

Drupal user roles export as Grav groups in a `groups.yaml` file at `config/groups.yaml`. Some notes about the role exporting:

* Each Drupal role is converted to the Grav group `drupal_<ROLE_WITH_UNDERSCORES>` (e.g. `authenticated user` becomes `drupal_authenticated_user`).
* The `drupal_administrator` group receives `admin.super` access along with `admin.login` access.
* The `drupal_authenticated_user` group receives `admin.login` access.
* All accounts receive the "drupal_authenticated_user" group.
* Drupal users with administrator roles receive the `drupal_administrator` group.

### Importing User Roles

Copy the `EXPORT/config` folder to `users/config`.

## Exporting Content Types from Drupal

### Command

`drush grav_export_content_types` or `drush gravect` will generate Grav blueprints and html.twig files.

### Results

For every defined field type, `drush gravect` will attempt to create compatible blueprint and matching html.twig file for each Drupal content type.  The files will be exported to `EXPORT/themes/drupal_export/blueprints` and `EXPORT/themes/drupal_export/templates` respectively.

### Limitations

#### Field "number_integer"

Cardinality in many Grav fields only support one value.  Only the first Drupal entry is exported.

#### Field "addressfield"

Grav has no concept of an address field.  Drupal field data is exported as an `array` form type.

### Importing Content Types to Grav

Copy the `EXPORT/themes/drupal_export/blueprints` and `EXPORT/themes/drupal_export/templates` folder to the active theme in Grav.  The admin plugin should now provide extra options for each content type and related fields.

Note: While field content is added to Grav page headers, display of those fields is **not** exported from Drupal.  The html.twig file will need to be modified in order to display any additional fields (besides main body content).

### Exporting Nodes from Drupal

### Command

`drush grav_export_nodes` or `drush graven` will generate Grav users and groups.

### Results

* Each node is exported to its own folder structure under `EXPORT/pages`, based on Drupal's url alias and content type (i.e. `pages/content/my_first_page/page.yaml` or `pages/content/cool_article/article.yaml`).
* Drupal field data is stored in the header of the page.
* Files are stored in `EXPORT/data/files/`, following a Drupal-like storage model.
* In the drush output, extra taxonomy terms may be outputted.  Copy these to Grav's `user/config/site.yaml` file under the taxonomy key.

### Importing Nodes to Grav Pages

Copy the `EXPORT/data` and `EXPORT/pages` folders to the `user directory in Grav. 

## To Do

* Finish adding all the basic Drupal fields to node and content type exporters.
* Use batch API to export nodes, in the event there are a lot of nodes.
* Add options for exporting select content (1 node, 1 user, 1 content type, etc.)
* Add ability to perform cardinality on Drupal fields (Grav fieldsets?)
