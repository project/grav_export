<?php

/**
 * @file
 * Initial drush file for grav_export. Houses all drush callouts.
 */

require 'vendor/autoload.php';


/**
 * @file
 * Drush commands for exporting Drupal content to GravCMS.
 */

/**
 * Implements hook_drush_command().
 */
function grav_export_drush_command() {
  $commands['grav_export_all'] = array(
    'callback' => 'drush_grav_export_all',
    'description' => 'Exports Drupal content to GravCMS files.',
    'aliases' => array('gravea'),
    'examples' => array(
      'drush grav_export_all' => 'Exports Drupal content to Grav files.',
      'drush gravea' => 'Exports Drupal content to Grav files.',
    ),
  );
  $commands['grav_export_users'] = array(
    'callback' => 'drush_grav_export_users',
    'description' => 'Exports Drupal users to GravCMS yaml account files.',
    'aliases' => array('graveu'),
    'options' => array(
      'all' => 'Exports all Drupal users, even those currently disabled.',
    ),
    'examples' => array(
      'drush grav_export_users' => 'Exports Drupal users to Grav account yaml files.',
      'drush grav_export_users --all' => 'Exports all users, even those currently disabled/ blocked.',
      'drush graveu' => 'Exports Drupal users to Grav account yaml files.',
      'drush graveu --all' => 'Exports all users, even those currently disabled/ blocked.',
    ),
  );
  $commands['grav_export_roles'] = array(
    'callback' => 'drush_grav_export_roles',
    'description' => 'Exports Drupal roles to a Grav groups.yaml file.',
    'aliases' => array('graver'),
    'examples' => array(
      'drush grav_export_roles' => 'Exports Drupal roles to a Grav groups.yaml file.',
      'drush graver' => 'Exports Drupal roles to a Grav groups.yaml file.',
    ),
  );
  $commands['grav_export_content_types'] = array(
    'callback' => 'drush_grav_export_content_types',
    'description' => 'Exports Drupal content types to a Grav blueprints.',
    'aliases' => array('gravect'),
    'examples' => array(
      'drush grav_export_content_types' => 'Exports Drupal content types to a Grav theme with blueprints.',
      'drush gravect' => 'Exports Drupal content types to a Grav theme with blueprints.',
    ),
  );
  $commands['grav_export_nodes'] = array(
    'callback' => 'drush_grav_export_nodes',
    'description' => 'Exports Drupal nodes to Grav files.',
    'aliases' => array('graven'),
    'examples' => array(
      'drush grav_export_nodes' => 'Exports Drupal nodes to Grav pages.',
      'drush graven' => 'Exports Drupal roles to Grav pages.',
    ),
  );
  return $commands;
}

/**
 * Converts a Drupal role to a Grav group.
 *
 * @param string $role_name
 *   Drupal role to be converted for Grav group name.
 *
 * @return string
 *   Grav compatible group name.
 */
function convert_role_dupal_to_grav($role_name) {
  return preg_replace('/\ /', '_', $role_name);
}

/**
 * Converts a Drupal username to Grav.
 *
 * @param string $user
 *   Drupal username to be converted.
 *
 * @return string
 *   Grav username.
 */
function convert_username_dupal_to_grav($user) {
  // Default Grav settings. Make sure to reflect changes in Grav's system.yaml.
  $user_char_mim_limit = 4;
  $user_char_max_limit = 16;

  $username = $user->name;

  mb_strtolower($username);

  // Replace invalid characters with underscore.
  $patterns = array(
    'space' => '/\ /',
    'period' => '/\./',
    'apostrophe' => '/\'/',
  );
  $replacements = array(
    'space' => '_',
    'period' => '_',
    'apostrophe' => '_',
  );
  $username = preg_replace($patterns, $replacements, $username);

  // Pad short usernames.
  if (strlen($username) < $user_char_mim_limit) {
    $username = $username . $user->uid;
    $username = str_pad($username, $user_char_mim_limit, "_");
  }

  // Trim long usernames.
  if (strlen($username) > $user_char_max_limit) {
    $uid_length = strlen($user->uid);
    $username = substr($username, 0, ($user_char_max_limit - $uid_length));
    $username .= $user->uid;
  }

  return $username;
}

/**
 * Generates a random string.
 *
 * Generate a random string, using a cryptographically secure
 * pseudorandom number generator (random_int).
 * For PHP 7, random_int is a PHP core function
 * For PHP 5.x, depends on https://github.com/paragonie/random_compat.
 *
 * @param int $length
 *   How many characters do we want?
 * @param string $keyspace
 *   A string of all possible characters to select from.
 *
 * @return string
 *   Pseudorandom string.
 *
 * @throws \Exception
 */
function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
  $str = '';
  $max = mb_strlen($keyspace, '8bit') - 1;
  for ($i = 0; $i < $length; ++$i) {
    $str .= $keyspace[random_int(0, $max)];
  }
  return $str;
}
