<?php

/**
 * @file
 * Handles drush command graven.
 */

use League\HTMLToMarkdown\HtmlConverter;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Drush command logic to export nodes.
 */
function drush_grav_export_nodes() {
  drush_print("");
  drush_print("Beginning node export.");

  $export_folder = "public://grav_export/user-" . date('Ymd') . "/";
  $pages_export_folder = $export_folder . "pages/";
  $files_export_folder = $export_folder . "data/files/";
  file_prepare_directory($pages_export_folder, FILE_CREATE_DIRECTORY);
  file_prepare_directory($files_export_folder, FILE_CREATE_DIRECTORY);

  $nids_query = db_select('node', 'n');
  $nids_query->fields('n', array('nid'));
  $nids = $nids_query->execute()->fetchCol();
  if (!$nids) {
    drush_print("No nodes to export");
    return;
  }

  // Creates a new progress bar.
  $output = new ConsoleOutput();
  $progress_bar = new ProgressBar($output, count($nids));
  $progress_bar->start();

  // todo: Refactor to use the batch API to avoid large exports.
  foreach ($nids as $nid) {
    $progress_bar->advance();
    $node = node_load($nid);

    // Create page folder.
    $path_alias = db_select('url_alias', 'ua')
      ->fields('ua', array('alias'))
      ->condition('ua.source', 'node/' . $nid, '=')
      ->orderBy('ua.pid', 'DESC')
      ->execute()->fetchCol();

    $node_folder = $pages_export_folder . $path_alias[0];
    file_prepare_directory($node_folder, FILE_CREATE_DIRECTORY);

    // Reset frontmatter.
    $frontmatter = NULL;

    // Set header information.
    $header = NULL;
    $header['nid'] = $node->nid;
    $header['title'] = $node->title;
    $header['date'] = date('H:i d-m-Y', $node->created);
    $header['published'] = $node->status;

    // todo: add route aliases, overrides, default per
    // https://learn.getgrav.org/content/routing#page-level-route-overrides.
    // Find other fields for this node.
    $fields = field_info_instances('node', $node->type);

    /*
     * The following loop goes through each node, and then each designated
     * field on the node, exporting the content to Grav formatted header and
     * frontmatter.
     */
    foreach ($fields as $field_key => $field) {
      // todo: identify other fields and add below.
      $field_name = $field['field_name'];

      // Skip field if empty.
      if (count($node->$field_name[LANGUAGE_NONE]) == 0) {
        continue;
      }

      $field_info = field_info_field_by_id($field['field_id']);

      switch ($field_info['type']) {

        case "addressfield":
          $address = $node->$field_name[LANGUAGE_NONE][0];
          $header[$field_name]['name_line'] = $address['name_line'];
          $header[$field_name]['thoroughfare'] = $address['thoroughfare'];
          $header[$field_name]['locality'] = $address['locality'];
          $header[$field_name]['administrative_area'] = $address['administrative_area'];
          $header[$field_name]['country'] = $address['country'];
          $header[$field_name]['postal_code'] = $address['postal_code'];
          break;

        case "bible_field":
          $bibleref = $node->$field_name[LANGUAGE_NONE][0];
          $header[$field_name] = array(
            array(
              "version" => "ESV",
              "book" => ucwords($bibleref['book_name']),
              "chapter" => $bibleref['chapter_start'],
            ),
          );

          break;

        case "datetime":
          $field_time = strtotime($node->$field_name[LANGUAGE_NONE][0]['value']);
          $header[$field_name] = date('h:i d-m-Y', $field_time);
          break;

        case "email":
          $header[$field_name] = $node->$field_name[LANGUAGE_NONE][0]['email'];
          break;

        case "file":
          // todo: Finish file field export.
          // break;.
        case "image":
          $images = $node->$field_name[LANGUAGE_NONE];
          foreach ($images as $image) {
            $media_export_folder = $files_export_folder . $field['settings']['file_directory'] . "/";

            file_prepare_directory($media_export_folder, FILE_CREATE_DIRECTORY);
            $image_name = $image['filename'];
            $grav_image = "user/data/files/" . $field['settings']['file_directory'] . '/' . $image_name;

            // Save image to media export folder.
            $drupal_file = file_load($image['fid']);
            file_copy($drupal_file, $media_export_folder . $image_name, FILE_EXISTS_REPLACE);

            // Page header information and metadata.
            $header[$field_name][$grav_image]['name'] = $image_name;
            $header[$field_name][$grav_image]['type'] = file_get_mimetype($node->field_image[LANGUAGE_NONE][0]['uri']);
            $header[$field_name][$grav_image]['size'] = $drupal_file->filesize;
            $header[$field_name][$grav_image]['path'] = $grav_image;
            $header[$field_name][$grav_image]['fid'] = $drupal_file->fid;
            $meta_name = $image_name . '.meta.yaml';
            $alt_text = $node->$field_name[LANGUAGE_NONE][0]['alt'];
            $title_text = $node->$field_name[LANGUAGE_NONE][0]['title'];
            $metadata_content = "image:\nalt_text: '" . $alt_text . "'\ntitle_text: '" . $title_text . "'\n";
            file_save_data($metadata_content, $media_export_folder . $meta_name, FILE_EXISTS_REPLACE);
          }
          break;

        case "list_boolean":
        case "list_integer":
        case "list_float":
        case "list_text":
          $header[$field_name] = $node->$field_name[LANGUAGE_NONE][0]['value'];
          break;

        case "link_field":
          $header[$field_name] = $node->$field_name[LANGUAGE_NONE][0]['url'];
          break;

        case "number_decimal":
        case "number_integer":
          $header[$field_name] = $node->$field_name[LANGUAGE_NONE][0]['value'];
          break;

        case "taxonomy_term_reference":
          $tids = $node->$field_name[LANGUAGE_NONE];
          foreach ($tids as $tid) {
            $term = taxonomy_term_load($tid['tid']);
            $taxonomy = taxonomy_vocabulary_load($term->vid);
            $header['taxonomy'][$taxonomy->machine_name][$term->name] = $term->name;
          }
          break;

        case "text":
        case "text_long":
          $header[$field_name] = $node->$field_name[LANGUAGE_NONE][0]['safe_value'];
          break;

        case "text_with_summary":
          // Convert body text to markdown frontmatter.
          // todo: Figure out how to add the summary.
          $converter = new HtmlConverter();
          $html = $node->body[LANGUAGE_NONE][0]['safe_value'];
          $frontmatter = $converter->convert($html);
          break;

        default:
          drush_print("\nMissing field definition: '" . $field_info['type'] . "' (" . $field['field_name'] . ") in '$node->title (node/$node->nid)'.");
          $missing = TRUE;
      }
    }

    $full_header = "---\n" . Yaml::dump($header, 20, 4) . "---\n";
    $page_output = $full_header . $frontmatter;
    $target = $node_folder . "/" . $node->type . ".md";
    file_save_data($page_output, $target, FILE_EXISTS_REPLACE);
  }

  drush_print("\nSave Complete!  " . count($nids) . " nodes exported");
  if (taxonomy_get_vocabularies()) {
    drush_print("");
    drush_print("Don't forget to append the following taxonomy vocabularies to Grav in site.yaml");
    drush_print("");
    drush_print("taxonomies:");
    foreach (taxonomy_get_vocabularies() as $vocab) {
      drush_print("  - " . $vocab->machine_name);
    }
  }
  if ($missing) {
    drush_print("\nSome content may be missing in the exported page due to missing field definitions.\n");
  }
}
