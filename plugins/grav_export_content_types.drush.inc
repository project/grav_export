<?php

/**
 * @file
 * Handles drush command gravect.
 */

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Drush command logic to export content types.
 */
function drush_grav_export_content_types() {
  drush_print("");
  drush_print("Beginning content types export.");
  $export_folder = "public://grav_export/user-" . date('Ymd') . "/";
  $files_export_folder = $export_folder . "data/files/";
  $theme_export_folder = $export_folder . "themes/drupal-export/";
  $templates_export_folder = $export_folder . "themes/drupal-export/templates/";
  $blueprints_export_folder = $export_folder . "themes/drupal-export/blueprints/";
  file_prepare_directory($templates_export_folder, FILE_CREATE_DIRECTORY);
  file_prepare_directory($blueprints_export_folder, FILE_CREATE_DIRECTORY);

  $content_types = node_type_get_types();

  // Creates a new progress bar.
  $output = new ConsoleOutput();
  $progress_bar = new ProgressBar($output, count($content_types));
  $progress_bar->start();

  foreach ($content_types as $content_type) {
    $progress_bar->advance();
    $blueprint_component = drupal_get_path('module', 'grav_export') . "/grav_components/contentType_blueprint.yaml";
    $blueprint = Yaml::parseFile($blueprint_component);
    $blueprint['title'] = $content_type->name;

    // Reset $new_fields for each content type.
    $new_fields = NULL;

    $fields = field_info_instances('node', $content_type->type);
    foreach ($fields as $field_key => $field) {
      // todo: identify other fields and add below.
      $field_name = $field['field_name'];
      $field_info = field_info_field_by_id($field['field_id']);

      switch ($field_info['type']) {
        case "addressfield":
          $new_fields["header." . $field_name] = array(
            "label" => $field['label'],
            "type" => "array",
            "help" => strip_tags($field['description']),
            "placeholder_key" => 'PLUGIN_ADMIN.METADATA_KEY',
            "placeholder_value" => 'PLUGIN_ADMIN.METADATA_VALUE',
          );
          break;

        case "bible_field":
          $call = '\Grav\Plugin\BiblerefPlugin::getBookOptions';
          $new_fields["header." . $field_name] = array(
            "name" => "bible_field",
            "type" => "list",
            "style" => "vertical",
            "label" => "Bible Reference(s)",
            "fields" => array(
              ".version" => array(
                "type" => "select",
                "size" => "small",
                "label" => "Bible Version",
                "help" => "Bible version for external links to biblegateway.com",
                "classes" => "fancy",
                "options" => array(
                  "ESV" => "ESV",
                  "KJV" => "KJV",
                  "NIV" => "NIV",
                ),
              ),
              ".book" => array(
                "type" => "select",
                "size" => "medium",
                "label" => "Book",
                '@data-options' => '\Grav\Plugin\BiblerefPlugin::getBookOptions',
              ),
              ".chapter" => array(
                "type" => "select",
                "size" => "medium",
                "label" => "Chapter",
                '@data-options' => '\Grav\Plugin\BiblerefPlugin::getBookChapterOptions',
              ),
            ),
          );
          break;

        case "datetime":
          $new_fields["header." . $field_name] = array(
            "label" => $field['label'],
            "type" => "datetime",
            "help" => strip_tags($field['description']),
          );
          break;

        case "email":
          $new_fields["header." . $field_name] = array(
            "label" => $field['label'],
            "type" => "email",
            "help" => strip_tags($field['description']),
          );
          break;

        case "file":
          $new_fields["header." . $field_name] = array(
            "label" => $field['label'],
            "type" => "file",
            "help" => strip_tags($field['description']) . " | Available file types: " . $field['settings']['file_extensions'],
            "destination" => "user/data/files/" . $field['settings']['file_directory'],
          );
          $extensions = explode(" ", $field['settings']['file_extensions']);

          foreach ($extensions as $extension) {
            $new_fields["header." . $field_name]["accept"][] = $extension;
          }

          if ($field_info['cardinality'] != 1) {
            $new_fields["header." . $field_name]['multiple'] = TRUE;
          }
          else {
            $new_fields["header." . $field_name]['multiple'] = FALSE;
          }
          break;

        case "image":
          $new_fields["header." . $field_name] = array(
            "label" => $field['label'],
            "type" => "file",
            "help" => strip_tags($field['description']) . " | Available file types: " . $field['settings']['file_extensions'],
            "destination" => "user/data/files/" . $field['settings']['file_directory'],
            "accept" => array('image/*'),
          );
          if ($field_info['cardinality'] != 1) {
            $new_fields["header." . $field_name]['multiple'] = TRUE;
          }
          else {
            $new_fields["header." . $field_name]['multiple'] = FALSE;
          }
          $extensions = explode(" ", $field['settings']['file_extensions']);
          foreach ($extensions as $extension) {
            $new_fields["header." . $field_name]["accept"][] = $extension;
          }
          break;

        case "link_field":
          $new_fields["header." . $field_name] = array(
            "help" => $field['description'],
            "label" => $field['label'],
            "type" => "text",
          );
          break;

        case "list_boolean":
          $new_fields["header." . $field_name] = array(
            "help" => $field['description'],
            "label" => $field['label'],
            "type" => "toggle",
            "options" => array(
              "1" => "True",
              "0" => "False",
            ),
            "highlight" => "1",
            "validate" => array(
              "type" => "bool",
            ),
          );
          break;

        case "list_integer":
        case "list_float":
        case "list_text":
          $new_fields["header." . $field_name] = array(
            "help" => $field['description'],
            "label" => $field['label'],
            "type" => "select",
            "default" => 'default',
          );

          // Create a default select option.
          $field_options = $field_info['settings']['allowed_values'];
          $default_option = array('default' => '-- Select an Option --');
          $new_fields["header." . $field_name]['options'] = array_merge($default_option, $field_options);

          break;

        case "number_decimal":
          $new_fields["header." . $field_name] = array(
            "help" => $field['description'],
            "label" => $field['label'],
            "type" => "text",
            "validate" => [
              "type" => "number",
            ],
          );
          break;

        case "number_integer":
          $new_fields["header." . $field_name] = array(
            "help" => $field['description'],
            "label" => $field['label'],
            "type" => "text",
            "validate" => [
              "type" => int,
            ],
          );
          break;

        case "taxonomy_term_reference":
          // Nothing needs to be done for taxonomy terms as Grav includes
          // this on the Options tab.
          break;

        case "text":
          $new_fields["header." . $field_name] = array(
            "help" => $field['description'],
            "label" => $field['label'],
            "type" => "text",
          );
          break;

        case "text_long":
          $new_fields["header." . $field_name] = array(
            "help" => $field['description'],
            "label" => $field['label'],
            "type" => "textarea",
            "rows" => $field['widget']['settings']['rows'],
          );
          break;

        case "text_with_summary":
          // Drupal body field belongs in frontmatter.
          // Don't create a field for it.
          if ($field_name == 'body') {
            break;
          }
          // todo: Finish text_with_summary export.
          break;

        default:
          drush_print("\nMissing field definition: '" . $field_info['type'] . "' (" . $field['field_name'] . ") in '$content_type->name'.");
          $missing = TRUE;
      }
    }

    if ($new_fields) {
      $blueprint['form']['fields']['tabs']['fields']['content']['fields'] = $new_fields;
    }
    else {
      unset($blueprint['form']);
    }

    // Output to yaml file.
    $yaml_output = Yaml::dump($blueprint, 20, 4);
    file_save_data($yaml_output, $blueprints_export_folder . $content_type->type . ".yaml", FILE_EXISTS_REPLACE);

    $template_content = "{% extends 'partials/base.html.twig' %}\n\n{% block content %}\n    {{ page.content }}\n{% endblock %}\n";
    file_save_data($template_content, $templates_export_folder . $content_type->type . ".html.twig", FILE_EXISTS_REPLACE);

    $theme_files = array(
      'blueprints.yaml',
      'CHANGELOG.md',
      'drupal-export.php',
      'drupal-export.yaml',
      'LICENSE',
      'README.md',
      'screenshot.jpg',
      'thumbnail.jpg',
    );
    $theme_files_path = drupal_get_path('module', 'grav_export') . "/grav_components/";
    foreach ($theme_files as $theme_file) {
      copy($theme_files_path . $theme_file, $theme_export_folder . $theme_file);
    }
  }
  $progress_bar->finish();
  drush_print("\n\nSave Complete!  " . count($content_types) . " content types exported.");
  drush_print("Drupal Export Grav theme is located in $theme_export_folder");
  if ($missing) {
    drush_print("\nSome content may be missing in the exported page due to missing field definitions.\n");
  }
}
