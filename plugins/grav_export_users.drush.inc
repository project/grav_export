<?php

/**
 * @file
 * Handles drush command graveu.
 */

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Drush command logic to export users.
 */
function drush_grav_export_users() {
  drush_print("");
  drush_print("Beginning user export.");
  $user_filter_all = drush_get_option('all');
  $export_folder = "public://grav_export/user-" . date('Ymd') . "/accounts/";
  $password_length = 16;

  $user_query = db_select('users', 'u');
  $user_query->fields('u', array('uid'));
  $user_query->condition('u.uid', 0, '!=');
  if (!$user_filter_all) {
    $user_query->condition('u.status', TRUE);
  }
  $uids = $user_query->execute()->fetchCol();
  if (!$uids) {
    drush_print('No users exported');
    return;
  }
  file_prepare_directory($export_folder, FILE_CREATE_DIRECTORY);

  // Creates a new progress bar.
  $output = new ConsoleOutput();
  $progress_bar = new ProgressBar($output, count($uids));
  $progress_bar->start();

  foreach ($uids as $key => $uid) {
    $progress_bar->advance();
    $user = user_load($uid);
    // Reset account.
    $account_content = NULL;
    $account_content['email'] = $user->mail;
    $account_content['drupal']['uid'] = $user->uid;
    $account_content['fullname'] = $user->name;
    $account_content['title'] = NULL;
    if ($user->status == TRUE) {
      $account_content['state'] = "enabled";
    }
    else {
      $account_content['state'] = "disabled";
    }
    if ($user->language != NULL && $user->language != 'und') {
      $account_content['language'] = $user->language;
    }
    else {
      $account_content['language'] = "en";
    }
    foreach ($user->roles as $role) {
      $account_content['groups'][] = "drupal_" . convert_role_dupal_to_grav($role);
    }
    $account_content['password'] = random_str($password_length);
    $account = Yaml::dump($account_content, 20, 4);
    $account .= "login_attempts: {  }";
    $filename = convert_username_dupal_to_grav($user);
    file_save_data($account, $export_folder . $filename . '.yaml', FILE_EXISTS_REPLACE);
  }
  drush_print("");
  drush_print("Saved Complete!  " . count($uids) . " user accounts exported to $export_folder");
}
