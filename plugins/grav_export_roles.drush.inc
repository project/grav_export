<?php

/**
 * @file
 * Handles drush command graver.
 */

use Symfony\Component\Yaml\Yaml;

/**
 * Drush command logic to export roles.
 */
function drush_grav_export_roles() {
  drush_print("");
  drush_print("Beginning role export.");
  $export_folder = "public://grav_export/user-" . date('Ymd') . "/config";
  file_prepare_directory($export_folder, FILE_CREATE_DIRECTORY);

  $role_query = db_select('role', 'r');
  $role_query->fields('r');
  $role_query->condition('r.name', 'anonymous user', '!=');
  $roles = $role_query->execute()->fetchAll();
  foreach ($roles as $key => $role) {
    $role_name = "drupal_" . convert_role_dupal_to_grav($role->name);
    $groups[$role_name]['icon'] = "cog";
    $groups[$role_name]['readableName'] = convert_role_dupal_to_grav($role->name);
    $groups[$role_name]['description'] = "Exported Drupal role " . convert_role_dupal_to_grav($role->name);
    $groups[$role_name]['access']['site']['login'] = TRUE;

    // Grant specific Grav admin access to Drupal roles.
    if ($role->name == 'authenticated user') {
      $groups[$role_name]['access']['admin'] = array(
        "login" => TRUE,
      );
    }
    if ($role->name == 'administrator') {
      $groups[$role_name]['access']['admin'] = array(
        "login" => TRUE,
        "super" => TRUE,
      );
    }

  }

  $group_content = Yaml::dump($groups, 20, 4);

  drush_print("Saving role export to $export_folder");
  file_save_data($group_content, $export_folder . '/groups.yaml', FILE_EXISTS_REPLACE);
  drush_print("Save Complete!  " . count($roles) . " roles exported");
}
