<?php

/**
 * @file
 * Handles drush command gravea.
 */

/**
 * Drush command logic wrapper to export all active content.
 */
function drush_grav_export_all() {
  drush_grav_export_users();
  drush_grav_export_roles();
  drush_grav_export_content_types();
  drush_grav_export_nodes();
}
